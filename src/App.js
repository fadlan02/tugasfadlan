import React from 'react';
import './App.css';
import Home from './Components/Home';
import Header from './Components/layout/Header';
import { Switch, Route } from 'react-router-dom';
import AddData from './Components/AddData';
import EditData from './Components/EditData';



function App() {
  return (
    <div class="app">
      <Header />
      <Switch>
        <Route path="/add" component={AddData} />
        <Route path="/edit" component={EditData} />
        <Route path="/" component={Home} />

      </Switch>

      {/* <Table /> */}
    </div>
  );
}

export default App;
