import React, { Component } from 'react';

export default class SearchByName extends Component {
    render() {
        return (
            <div className="search">
                <input type="text" placeholder=" Cari nama ..." className="srcInput" />
                <button className="btn btn-outline-primary btnSearch">Cari</button>
            </div >
        )

    }
}