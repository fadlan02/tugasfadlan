import React, { Component } from 'react';
import Axios from 'axios';
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import Search from './SearchByName';
export default class Home extends Component {
    state = {
        user: [],
    }

    handleDelete = id => {
        Axios.delete(`http://localhost:8282/delete/` + id)
            .then(res => {
                console.log(res);
                console.log(res.data);
                window.location.reload();
            })
    }



    componentDidMount() {
        Axios.get('http://localhost:8282/employees')
            .then(res => {
                const user = res.data;
                this.setState({ user });
            })
    }

    render() {
        return (
            <div className="home">
                <header />
                <Search />
                <Link to='/add'>
                    <Button className="addBtn" variant="contained" color="primary">Add Data</Button>
                </Link>
                <br></br>
                <table border="1" cellSpacing="0" cellPadding="5" id="tes" className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Jenis-kelamin</th>
                            <th>Tempat-lahir</th>
                            <th>Tanggal-lahir</th>
                            <th>Kecamatan</th>
                            <th>Kelurahan</th>
                            <th>RT</th>
                            <th>RW</th>
                            <th>Pekerjaan</th>
                            <th>Setting</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.user.map(user =>
                                <tr key={user.id}>
                                    <td>{user.nik}</td>
                                    <td>{user.name}</td>
                                    <td>{user.alamat}</td>
                                    <td>{user.jenis_kelamin}</td>
                                    <td>{user.tempat_lahir}</td>
                                    <td>{user.tanggal_lahir}</td>
                                    <td>{user.kecamatan}</td>
                                    <td>{user.kelurahan}</td>
                                    <td>{user.rt}</td>
                                    <td>{user.rw}</td>
                                    <td>{user.pekerjaan}</td>

                                    <td>
                                        <Link to='/edit'>
                                            <button className="btn btn-warning">Edit </button>
                                        </Link>
                                        <button className="btn btn-danger" onClick={() => this.handleDelete(user.id)}>Hapus</button></td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>

            </div>
        );
    }
}
