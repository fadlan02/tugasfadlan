import React from 'react';
import axios from 'axios';
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';

export default class employees extends React.Component {
    state = {
        nik: ' ',
        name: ' ',
        alamat: ' ',
        tempat_lahir: ' ',
        tanggal_lahir: ' ',
        jenis_kelamin: '',
        agama: '',
        kecamatan: '',
        kelurahan: '',
        rt: '',
        rw: '',
        pekerjaan: '',
    }
    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit = event => {
        event.preventDefault();

        console.log(this.state)
        axios.post(`http://localhost:8282/update`, this.state)
            .then(res => {
                window.location.reload();
                console.log(res)
                console.log(res.data);
            })
    }

    render() {
        return (
            <div className="addData">
                <div className="container">
                    <h1 className="judul"> Edit Data </h1>
                    <form onSubmit={this.handleSubmit}>
                        <label>NIK</label><br></br>
                        <input type="number" className="form-control" id="nik" name="nik" onChange={this.handleChange} /><br></br>

                        <label>Nama</label><br></br>
                        <input type="text" className="form-control" id="name" name="name" onChange={this.handleChange} /><br></br>

                        <label>Jenis Kelamin</label><br></br>
                        <input type="text" className="form-control" id="jenis_kelamin" name="jenis_kelamin" onChange={this.handleChange} /><br></br>


                        <label>Tempat lahir</label><br></br>
                        <input type="text" className="form-control" id="tempat_lahir" name="tempat_lahir" onChange={this.handleChange} /><br></br>

                        <label>Tanggal lahir</label><br></br>
                        <input type="text" className="form-control" id="tanggal_lahir" name="tanggal_lahir" onChange={this.handleChange} /><br></br>

                        <label>Alamat</label><br></br>
                        <input type="text" className="form-control" id="alamat" name="alamat" onChange={this.handleChange} /><br></br>

                        <label>Agama</label><br></br>
                        <input type="text" className="form-control" id="agama" name="agama" onChange={this.handleChange} /><br></br>

                        <label>Kecamatan</label><br></br>
                        <input type="text" className="form-control" id="kecamatan" name="kecamatan" onChange={this.handleChange} /><br></br>

                        <label>Kelurahan</label><br></br>
                        <input type="text" className="form-control" id="kelurahan" name="kelurahan" onChange={this.handleChange} /><br></br>

                        <label>RT</label><br></br>
                        <input type="number" className="form-control" id="rt" name="rt" onChange={this.handleChange} /><br></br>
                        <label>RW</label><br></br>
                        <input type="number" className="form-control" id="rw" name="rw" onChange={this.handleChange} />
                        <br></br>
                        <label>Pekerjaan</label><br></br>
                        <input type="text" className="form-control" id="pekerjaan" name="pekerjaan" onChange={this.handleChange} /><br></br>

                        <button className="btn btn-success">Tambah</button>
                        <Link to='/'>
                            <button className="btn btn-secondary">Back</button>
                        </Link>
                    </form>
                </div>
            </div>
        )
    }

}